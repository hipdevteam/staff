<?php 

namespace HipStaff;
use Pimple\Container;

class Plugin extends Container
{
	public function __construct()
	{
		$this['settings'] = function( $container ) {
			return new Settings();
		};
		
		$this['settings_page'] = function( $container ) {
			return new SettingsPage();
		};
		
		$this['api'] = function( $container ) {
			return new API();
		};
	}
	
	public function run()
	{
		add_action( 'admin_menu', function() {
			$this['settings_page']->addOptionsPage();
		} );
		
		add_action( 'rest_api_init', function() {
			$this['api']->addRoutes();
		} );
		
		add_action( 'admin_enqueue_scripts', function() {
			$this['settings_page']->enqueueAssets();
		} );

		add_action('add_meta_boxes', array($this, 'attach_staff_detail_meta_box'));
		add_action('save_post', array($this, 'save_staff_meta_box_information'));

		add_action('init', [ $this, 'update' ], 5);
		add_action('init', [ $this, 'register_post_type' ], 10);
		add_action('init',[$this,'staff_bb_module']);
	}
	
	public function attach_staff_detail_meta_box()
	{
		add_meta_box('register_staff_details_meta_box', __('Details'), array($this, 'render_staff_meta_box'), 'staff', 'normal', 'high', null);
	}

	public function save_staff_meta_box_information($post_id)
	{	
		$identifiers = array('staff_title', 'staff_email', 'staff_phone');

		for ($i = 0; $i < 3; $i++)
		{
			if (array_key_exists($identifiers[$i], $_POST))
			{
				update_post_meta($post_id, $identifiers[$i], $_POST[$identifiers[$i]]);
			}
		}
	}

	public function render_staff_meta_box($self)
	{
		$labels = array('Title', 'Email', 'Phone');
		$identifiers = array('staff_title', 'staff_email', 'staff_phone');

		$meta_data = null;
		if ($self->ID)
			$meta_data = get_post_meta($self->ID);

		echo '<div>';
			for ($i = 0; $i < 3; $i++)
			{
				echo '<div style="display: flex; margin: 0 0 12px;">';
					echo '<div style="width:25%; border-readius: 4px; line-height: 2;">';
						echo '<label for="staff_title" style="font-weight:600;">' . $labels[$i] . '</label>';
					echo '</div>';
					echo '<div style="width:75%; ">';
						echo '<input type = "text" size="30" id="' . $identifiers[$i] . '" name="'. $identifiers[$i] . '" value="' 
							. ($meta_data ? $meta_data[$identifiers[$i]][0] : "") . '" style="min-height: 30px; padding: 0 8px;">';
					echo '</div>';
				echo '</div>';
			}
		echo '</div>';
	}

	public function register_post_type()
	{
		$options = $this['settings']->getOptions();
		
		$labels = [
			'name'			=> $options['staff_plural_label'],
			'singular_name'	=> $options['staff_singular_label'],
			'add_new_item'  => 'Add New '.$options['staff_singular_label']
		];
		
		register_post_type( 'staff', [
			'labels'			=> $labels,
			'public'			=> true,
			'has_archive'		=> $options['staff_slug'],
			'rewrite'			=> [ 'slug' => $options['staff_slug'] ],
			'hierarchical'		=> true,
			'supports'			=> [
				'title',
				'editor',
				'thumbnail',
				'page-attributes',
				'excerpt',
				'revisions'
			]
		] );
		
		register_taxonomy( 'staff_category', [ 'staff' ], [
			'label'				=> $options['staff_cat_label'],
			'show_admin_column'	=> true,
			'hierarchical'		=> true,
			'rewrite'			=> [ 'slug' => $options['staff_cat_slug'] ]
		] );
	}

	public function staff_bb_module(){
		if ( class_exists( 'FLBuilder' ) ) {
			require_once $this['dir'].'/staff-bb-module/staff-bb-module.php';
		}
	}

	public function update()
	{
		$options = $this['settings']->getOptions();
		$saved_version = get_option('hip_staff_version') ? get_option('hip_staff_version'): '1.1.2';
		if (version_compare($saved_version, '1.1.2', '<=')) {
			if ($options['staff_label']) {
				$options['staff_singular_label'] = $options['staff_label'];
				$options['staff_plural_label'] = $options['staff_label'] . 's';
				$this['settings']->saveOptions($options);
			}
		}
		update_option('hip_staff_version', $this['version']);
	}
}
